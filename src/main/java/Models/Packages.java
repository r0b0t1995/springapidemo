package Models;

import jakarta.persistence.*;

@Table(name = "packages")
@Entity(name = "packages")
public class Packages {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int packagesId;

    @Column(name = "packegesName", nullable = false, unique = true)
    private String packegesName;

    @Column(name = "packegesPrice", nullable = false)
    private int packegesPrice;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "statusFk", referencedColumnName = "statusId")
    private Status status;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "roomFk", referencedColumnName = "roomId")
    private Rooms rooms;

    public int getPackagesId() {
        return packagesId;
    }

    public void setPackagesId(int packagesId) {
        this.packagesId = packagesId;
    }

    public String getPackegesName() {
        return packegesName;
    }

    public void setPackegesName(String packegesName) {
        this.packegesName = packegesName;
    }

    public int getPackegesPrice() {
        return packegesPrice;
    }

    public void setPackegesPrice(int packegesPrice) {
        this.packegesPrice = packegesPrice;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Rooms getRooms() {
        return rooms;
    }

    public void setRooms(Rooms rooms) {
        this.rooms = rooms;
    }
}
