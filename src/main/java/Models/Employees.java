package Models;

import jakarta.persistence.*;
import lombok.NoArgsConstructor;

@Entity(name = "employees")
@Table(name = "employees")
@NoArgsConstructor
public class Employees {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long employeeId;

    @Column(name = "employeesCardId", unique = true, nullable = false)
    private String employeeCardId;

    @Column(name = "employeesName", nullable = false)
    private String employeeName;

    @Column(name = "customerLastName", nullable = false)
    private String employeeLastName;

    @Column(name = "customerEmail")
    private String employeeEmail;

    @Column(name = "customerPhoneNumber")
    private int customerPhoneNumber;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "statusFk", referencedColumnName = "statusId")
    private Status status;

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeCardId() {
        return employeeCardId;
    }

    public void setEmployeeCardId(String employeeCardId) {
        this.employeeCardId = employeeCardId;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeLastName() {
        return employeeLastName;
    }

    public void setEmployeeLastName(String employeeLastName) {
        this.employeeLastName = employeeLastName;
    }

    public String getEmployeeEmail() {
        return employeeEmail;
    }

    public void setEmployeeEmail(String employeeEmail) {
        this.employeeEmail = employeeEmail;
    }

    public int getCustomerPhoneNumber() {
        return customerPhoneNumber;
    }

    public void setCustomerPhoneNumber(int customerPhoneNumber) {
        this.customerPhoneNumber = customerPhoneNumber;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
