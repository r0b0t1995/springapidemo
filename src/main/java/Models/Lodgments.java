package Models;

import jakarta.persistence.*;
import lombok.NoArgsConstructor;
import java.sql.Date;

@Entity(name = "lodgments")
@Table(name = "lodgments")
@NoArgsConstructor
public class Lodgments {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long lodgmentId;

    @Column(name = "firstDay")
    private Date firstDay;

    @Column(name = "lastDay")
    private Date lastDay;

    @Column(name = "totalPrice", nullable = false)
    private int totalPrice;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "customerFk", referencedColumnName = "customerId")
    private Customers customers;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "packagesFk", referencedColumnName = "packagesId")
    private Packages  packages;

    public Long getLodgmentId() {
        return lodgmentId;
    }

    public void setLodgmentId(Long lodgmentId) {
        this.lodgmentId = lodgmentId;
    }

    public Date getFirstDay() {
        return firstDay;
    }

    public void setFirstDay(Date firstDay) {
        this.firstDay = firstDay;
    }

    public Date getLastDay() {
        return lastDay;
    }

    public void setLastDay(Date lastDay) {
        this.lastDay = lastDay;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Customers getCustomers() {
        return customers;
    }

    public void setCustomers(Customers customers) {
        this.customers = customers;
    }

    public Packages getPackages() {
        return packages;
    }

    public void setPackages(Packages packages) {
        this.packages = packages;
    }
}
