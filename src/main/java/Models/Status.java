package Models;

import jakarta.persistence.*;
import lombok.NoArgsConstructor;

@Entity(name = "status")
@Table(name = "status")
@NoArgsConstructor
public class Status {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Byte idStatus;

    @Column(name = "nameStatus", unique = true, nullable = false)
    private String nameStatus;


    public Byte getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(Byte idStatus) {
        this.idStatus = idStatus;
    }

    public String getNameStatus() {
        return nameStatus;
    }

    public void setNameStatus(String nameStatus) {
        this.nameStatus = nameStatus;
    }
}
