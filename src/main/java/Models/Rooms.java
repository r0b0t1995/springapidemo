package Models;

import jakarta.persistence.*;
import lombok.NoArgsConstructor;

@Entity(name = "rooms")
@Table(name = "rooms")
@NoArgsConstructor
public class Rooms {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int roomId;

    @Column(name = "roomNumber", unique = true, nullable = false)
    private int roomNumber;

    @Column(name = "numberBeds", nullable = false)
    private byte numberBeds;

    @Column(name = "roomPrice", nullable = false)
    private int roomPrice;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "statusFk", referencedColumnName = "statusId")
    private Status status;

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public int getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(int roomNumber) {
        this.roomNumber = roomNumber;
    }

    public byte getNumberBeds() {
        return numberBeds;
    }

    public void setNumberBeds(byte numberBeds) {
        this.numberBeds = numberBeds;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public int getRoomPrice() {
        return roomPrice;
    }

    public void setRoomPrice(int roomPrice) {
        this.roomPrice = roomPrice;
    }
}
